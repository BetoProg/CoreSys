using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CoreSys.Data;
using CoreSys.Models;

namespace CoreSys.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Articles")]
    public class ArticlesController : Controller
    {
        private readonly SysContext _context;

        public ArticlesController(SysContext context)
        {
            _context = context;
        }

        // GET: api/Articles
        [HttpGet]
        public ArticlesList GetArticles(int pageIndex, int pageSize)
        {
            ArticlesList articlelist = new ArticlesList();
            int pgIndex = pageIndex - 1;
            var query = _context.Articles
                .Skip(pageSize * pgIndex).Take(pageSize);
            articlelist.ListArticles = query.ToList();
            articlelist.totalCount = _context.Articles.Count();
            return articlelist;
        }

        // GET: api/Articles/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetArticle(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var article = await _context.Articles
                .Include(c=>c.Category).Select(b=>
                    new ArticleDetails()
                    {
                        IdArticle =b.IdArticle,
                        Name=b.Name,
                        Code=b.Code,
                        Description=b.Description,
                        Stock=b.Stock,
                        PathImage=b.PathImage,
                        NameCategory=b.Category.Name
                    }).SingleOrDefaultAsync(b => b.IdArticle == id);

            if (article == null)
            {
                return NotFound();
            }

            return Ok(article);
        }

        // PUT: api/Articles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutArticle([FromRoute] int id, [FromBody] Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != article.IdArticle)
            {
                return BadRequest();
            }

            _context.Entry(article).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArticleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Articles
        [HttpPost]
        public async Task<IActionResult> PostArticle([FromBody] Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Articles.Add(article);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetArticle", new { id = article.IdArticle }, article);
        }

        // DELETE: api/Articles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteArticle([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var article = await _context.Articles.SingleOrDefaultAsync(m => m.IdArticle == id);
            if (article == null)
            {
                return NotFound();
            }

            _context.Articles.Remove(article);
            await _context.SaveChangesAsync();

            return Ok(article);
        }

        private bool ArticleExists(int id)
        {
            return _context.Articles.Any(e => e.IdArticle == id);
        }
    }
}