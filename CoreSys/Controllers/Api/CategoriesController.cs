using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CoreSys.Data;
using CoreSys.Models;

namespace CoreSys.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Categories")]
    public class CategoriesController : Controller
    {
        private readonly SysContext _context;

        public CategoriesController(SysContext context)
        {
            _context = context;
        }

        // GET: api/Categories
        [HttpGet]
        public CategoryList GetCategories(int pageIndex, int pageSize)
        {
            CategoryList categorieslist = new CategoryList();
            int pgIndex = pageIndex - 1;
            var query = _context.Categories
                .Skip(pageSize * pgIndex).Take(pageSize);
            categorieslist.ListCategory = query.ToList();
            categorieslist.TotalCount = _context.Categories.Count();

            return categorieslist;
        }

        // GET: api/Categories/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var category = await _context.Categories.SingleOrDefaultAsync(m => m.IdCategory == id);

            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }

        // PUT: api/Categories/5
        //[HttpPut("{id}")]
        public async Task<IActionResult> PutCategory([FromBody] Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cat = _context.Categories.SingleOrDefault(c => c.IdCategory == category.IdCategory);

            cat.IdCategory = category.IdCategory;
            cat.Name = category.Name;
            cat.Description = category.Description;
            cat.Active = category.Active;

            if (cat == null)
            {
                return BadRequest();
            }

            _context.Entry(cat).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        // POST: api/Categories
        [HttpPost]
        public async Task<IActionResult> PostCategory([FromBody] Category category)
        {
            
            if (!ModelState.IsValid)
            {
                object CategoryError=null;

                var messagesErrors = (from state in ModelState.Values
                                     from error in state.Errors
                                     select error.ErrorMessage).ToList();
                CategoryError = new
                {
                    Name=messagesErrors[0],
                    Active = messagesErrors[1],
                    Description = messagesErrors[2]
                };
                
                return BadRequest(CategoryError);
            }

            _context.Categories.Add(category);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCategory", new { id = category.IdCategory }, category);
        }

        // DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var category = await _context.Categories.SingleOrDefaultAsync(m => m.IdCategory == id);
            if (category == null)
            {
                return NotFound();
            }

            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();

            return Ok(category);
        }

        private bool CategoryExists(int id)
        {
            return _context.Categories.Any(e => e.IdCategory == id);
        }
    }
}