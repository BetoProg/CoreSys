﻿using CoreSys.Models;
using Microsoft.EntityFrameworkCore;

namespace CoreSys.Data
{
    public class SysContext:DbContext
    {
        public SysContext(DbContextOptions<SysContext> options)
            :base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Article> Articles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Entry> Entries { get; set; }

        public DbSet<Sale> Sales { get; set; }

        public DbSet<VoucherType> VoucherType { get; set; }

        public DbSet<DocumentType> DocumentType { get; set; }

        public DbSet<DetailEntry> DetailEntries { get; set; }

        public DbSet<DetailSale> DetailSales { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().ToTable("Category");
            modelBuilder.Entity<Article>().ToTable("Article");
            modelBuilder.Entity<Customer>().ToTable("Customer");
            modelBuilder.Entity<Entry>().ToTable("Entry");
            modelBuilder.Entity<Sale>().ToTable("Sale");
            modelBuilder.Entity<VoucherType>().ToTable("VoucherType");
            modelBuilder.Entity<DocumentType>().ToTable("DocumentType");
            modelBuilder.Entity<DetailEntry>().ToTable("DetailEntry");
            modelBuilder.Entity<DetailSale>().ToTable("DetailSale");
        }
    }
}
