﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CoreSys.Data;
using CoreSys.Models;

namespace CoreSys.Migrations
{
    [DbContext(typeof(SysContext))]
    [Migration("20170601034739_ContruccionBase")]
    partial class ContruccionBase
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CoreSys.Models.Article", b =>
                {
                    b.Property<int>("IdArticle")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(512);

                    b.Property<int>("IdCategory");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("PathImage")
                        .IsRequired()
                        .HasMaxLength(130);

                    b.Property<int>("Stock");

                    b.HasKey("IdArticle");

                    b.HasIndex("IdCategory");

                    b.ToTable("Article");
                });

            modelBuilder.Entity("CoreSys.Models.Category", b =>
                {
                    b.Property<int>("IdCategory")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(120);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("IdCategory");

                    b.ToTable("Category");
                });

            modelBuilder.Entity("CoreSys.Models.Customer", b =>
                {
                    b.Property<int>("IdCustomer")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasMaxLength(70);

                    b.Property<string>("City")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("Country")
                        .HasMaxLength(30);

                    b.Property<string>("CustomerType")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(80);

                    b.Property<int>("IdDocument");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(80);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Phone")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("PostalCode")
                        .IsRequired()
                        .HasMaxLength(5);

                    b.Property<string>("Region")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("IdCustomer");

                    b.HasIndex("IdDocument");

                    b.ToTable("Customer");
                });

            modelBuilder.Entity("CoreSys.Models.DetailEntry", b =>
                {
                    b.Property<int>("IdDetailEntry")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("IdArticle");

                    b.Property<int>("IdEntry");

                    b.Property<decimal>("PurchasePrice");

                    b.Property<int>("Quantity");

                    b.Property<decimal>("SalePrice");

                    b.HasKey("IdDetailEntry");

                    b.HasIndex("IdArticle");

                    b.HasIndex("IdEntry");

                    b.ToTable("DetailEntry");
                });

            modelBuilder.Entity("CoreSys.Models.DetailSale", b =>
                {
                    b.Property<int>("IdDetailSale")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Discount");

                    b.Property<int>("IdArticle");

                    b.Property<int>("IdSale");

                    b.Property<int>("Quantity");

                    b.Property<decimal>("SalePrice");

                    b.HasKey("IdDetailSale");

                    b.HasIndex("IdArticle");

                    b.HasIndex("IdSale");

                    b.ToTable("DetailSale");
                });

            modelBuilder.Entity("CoreSys.Models.DocumentType", b =>
                {
                    b.Property<int>("IdDocument")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DocumentName")
                        .IsRequired();

                    b.HasKey("IdDocument");

                    b.ToTable("DocumentType");
                });

            modelBuilder.Entity("CoreSys.Models.Entry", b =>
                {
                    b.Property<int>("IdEntry")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTime");

                    b.Property<int>("IdCustomer");

                    b.Property<int>("IdVoucherType");

                    b.Property<bool>("State");

                    b.Property<decimal>("Tax");

                    b.Property<string>("VoucherNumber")
                        .HasMaxLength(7);

                    b.Property<string>("VoucherSerie")
                        .HasMaxLength(20);

                    b.HasKey("IdEntry");

                    b.HasIndex("IdCustomer");

                    b.HasIndex("IdVoucherType");

                    b.ToTable("Entry");
                });

            modelBuilder.Entity("CoreSys.Models.Sale", b =>
                {
                    b.Property<int>("IdSale")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTime");

                    b.Property<int>("IdCustomer");

                    b.Property<int>("IdVoucherType");

                    b.Property<decimal>("SaleTotal");

                    b.Property<bool>("State");

                    b.Property<decimal>("Tax");

                    b.Property<string>("VoucherNumber")
                        .HasMaxLength(10);

                    b.Property<string>("VoucherSerie")
                        .HasMaxLength(7);

                    b.HasKey("IdSale");

                    b.HasIndex("IdCustomer");

                    b.HasIndex("IdVoucherType");

                    b.ToTable("Sale");
                });

            modelBuilder.Entity("CoreSys.Models.VoucherType", b =>
                {
                    b.Property<int>("IdVoucherType")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("VoucherName")
                        .IsRequired();

                    b.HasKey("IdVoucherType");

                    b.ToTable("VoucherType");
                });

            modelBuilder.Entity("CoreSys.Models.Article", b =>
                {
                    b.HasOne("CoreSys.Models.Category", "Category")
                        .WithMany("Articles")
                        .HasForeignKey("IdCategory")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CoreSys.Models.Customer", b =>
                {
                    b.HasOne("CoreSys.Models.DocumentType", "DocumentTypes")
                        .WithMany("Customer")
                        .HasForeignKey("IdDocument")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CoreSys.Models.DetailEntry", b =>
                {
                    b.HasOne("CoreSys.Models.Article", "Article")
                        .WithMany("DetailEntries")
                        .HasForeignKey("IdArticle")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CoreSys.Models.Entry", "Entry")
                        .WithMany("DetailEntries")
                        .HasForeignKey("IdEntry")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CoreSys.Models.DetailSale", b =>
                {
                    b.HasOne("CoreSys.Models.Article", "Article")
                        .WithMany("DetailSales")
                        .HasForeignKey("IdArticle")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CoreSys.Models.Sale", "Sale")
                        .WithMany("DetailSales")
                        .HasForeignKey("IdSale")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CoreSys.Models.Entry", b =>
                {
                    b.HasOne("CoreSys.Models.Customer", "Customers")
                        .WithMany("Entries")
                        .HasForeignKey("IdCustomer")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CoreSys.Models.VoucherType", "VoucherType")
                        .WithMany("Entries")
                        .HasForeignKey("IdVoucherType")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CoreSys.Models.Sale", b =>
                {
                    b.HasOne("CoreSys.Models.Customer", "Customers")
                        .WithMany("Sales")
                        .HasForeignKey("IdCustomer")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CoreSys.Models.VoucherType", "VoucherType")
                        .WithMany("Sales")
                        .HasForeignKey("IdVoucherType")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
