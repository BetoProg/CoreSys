﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CoreSys.Migrations
{
    public partial class ContruccionBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    IdCategory = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(maxLength: 120, nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.IdCategory);
                });

            migrationBuilder.CreateTable(
                name: "DocumentType",
                columns: table => new
                {
                    IdDocument = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DocumentName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentType", x => x.IdDocument);
                });

            migrationBuilder.CreateTable(
                name: "VoucherType",
                columns: table => new
                {
                    IdVoucherType = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VoucherName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoucherType", x => x.IdVoucherType);
                });

            migrationBuilder.CreateTable(
                name: "Article",
                columns: table => new
                {
                    IdArticle = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Code = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 512, nullable: false),
                    IdCategory = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    PathImage = table.Column<string>(maxLength: 130, nullable: false),
                    Stock = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Article", x => x.IdArticle);
                    table.ForeignKey(
                        name: "FK_Article_Category_IdCategory",
                        column: x => x.IdCategory,
                        principalTable: "Category",
                        principalColumn: "IdCategory",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    IdCustomer = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(maxLength: 70, nullable: false),
                    City = table.Column<string>(maxLength: 50, nullable: false),
                    Country = table.Column<int>(maxLength: 30, nullable: false),
                    CustomerType = table.Column<string>(maxLength: 20, nullable: false),
                    Email = table.Column<string>(maxLength: 80, nullable: false),
                    IdDocument = table.Column<int>(nullable: false),
                    LastName = table.Column<string>(maxLength: 80, nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Phone = table.Column<string>(maxLength: 10, nullable: false),
                    PostalCode = table.Column<string>(maxLength: 5, nullable: false),
                    Region = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.IdCustomer);
                    table.ForeignKey(
                        name: "FK_Customer_DocumentType_IdDocument",
                        column: x => x.IdDocument,
                        principalTable: "DocumentType",
                        principalColumn: "IdDocument",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Entry",
                columns: table => new
                {
                    IdEntry = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateTime = table.Column<DateTime>(nullable: false),
                    IdCustomer = table.Column<int>(nullable: false),
                    IdVoucherType = table.Column<int>(nullable: false),
                    State = table.Column<bool>(nullable: false),
                    Tax = table.Column<decimal>(nullable: false),
                    VoucherNumber = table.Column<string>(maxLength: 7, nullable: true),
                    VoucherSerie = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Entry", x => x.IdEntry);
                    table.ForeignKey(
                        name: "FK_Entry_Customer_IdCustomer",
                        column: x => x.IdCustomer,
                        principalTable: "Customer",
                        principalColumn: "IdCustomer",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Entry_VoucherType_IdVoucherType",
                        column: x => x.IdVoucherType,
                        principalTable: "VoucherType",
                        principalColumn: "IdVoucherType",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sale",
                columns: table => new
                {
                    IdSale = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateTime = table.Column<DateTime>(nullable: false),
                    IdCustomer = table.Column<int>(nullable: false),
                    IdVoucherType = table.Column<int>(nullable: false),
                    SaleTotal = table.Column<decimal>(nullable: false),
                    State = table.Column<bool>(nullable: false),
                    Tax = table.Column<decimal>(nullable: false),
                    VoucherNumber = table.Column<string>(maxLength: 10, nullable: true),
                    VoucherSerie = table.Column<string>(maxLength: 7, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sale", x => x.IdSale);
                    table.ForeignKey(
                        name: "FK_Sale_Customer_IdCustomer",
                        column: x => x.IdCustomer,
                        principalTable: "Customer",
                        principalColumn: "IdCustomer",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sale_VoucherType_IdVoucherType",
                        column: x => x.IdVoucherType,
                        principalTable: "VoucherType",
                        principalColumn: "IdVoucherType",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DetailEntry",
                columns: table => new
                {
                    IdDetailEntry = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdArticle = table.Column<int>(nullable: false),
                    IdEntry = table.Column<int>(nullable: false),
                    PurchasePrice = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    SalePrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetailEntry", x => x.IdDetailEntry);
                    table.ForeignKey(
                        name: "FK_DetailEntry_Article_IdArticle",
                        column: x => x.IdArticle,
                        principalTable: "Article",
                        principalColumn: "IdArticle",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DetailEntry_Entry_IdEntry",
                        column: x => x.IdEntry,
                        principalTable: "Entry",
                        principalColumn: "IdEntry",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DetailSale",
                columns: table => new
                {
                    IdDetailSale = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discount = table.Column<decimal>(nullable: false),
                    IdArticle = table.Column<int>(nullable: false),
                    IdSale = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    SalePrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetailSale", x => x.IdDetailSale);
                    table.ForeignKey(
                        name: "FK_DetailSale_Article_IdArticle",
                        column: x => x.IdArticle,
                        principalTable: "Article",
                        principalColumn: "IdArticle",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DetailSale_Sale_IdSale",
                        column: x => x.IdSale,
                        principalTable: "Sale",
                        principalColumn: "IdSale",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Article_IdCategory",
                table: "Article",
                column: "IdCategory");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_IdDocument",
                table: "Customer",
                column: "IdDocument");

            migrationBuilder.CreateIndex(
                name: "IX_DetailEntry_IdArticle",
                table: "DetailEntry",
                column: "IdArticle");

            migrationBuilder.CreateIndex(
                name: "IX_DetailEntry_IdEntry",
                table: "DetailEntry",
                column: "IdEntry");

            migrationBuilder.CreateIndex(
                name: "IX_DetailSale_IdArticle",
                table: "DetailSale",
                column: "IdArticle");

            migrationBuilder.CreateIndex(
                name: "IX_DetailSale_IdSale",
                table: "DetailSale",
                column: "IdSale");

            migrationBuilder.CreateIndex(
                name: "IX_Entry_IdCustomer",
                table: "Entry",
                column: "IdCustomer");

            migrationBuilder.CreateIndex(
                name: "IX_Entry_IdVoucherType",
                table: "Entry",
                column: "IdVoucherType");

            migrationBuilder.CreateIndex(
                name: "IX_Sale_IdCustomer",
                table: "Sale",
                column: "IdCustomer");

            migrationBuilder.CreateIndex(
                name: "IX_Sale_IdVoucherType",
                table: "Sale",
                column: "IdVoucherType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DetailEntry");

            migrationBuilder.DropTable(
                name: "DetailSale");

            migrationBuilder.DropTable(
                name: "Entry");

            migrationBuilder.DropTable(
                name: "Article");

            migrationBuilder.DropTable(
                name: "Sale");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "VoucherType");

            migrationBuilder.DropTable(
                name: "DocumentType");
        }
    }
}
