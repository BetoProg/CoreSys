﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreSys.Models
{
    public class Article
    {
        [Key]
        public int IdArticle { get; set; }

        public int IdCategory { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public int Stock { get; set; }

        [Required]
        [StringLength(512)]
        public string Description { get; set; }

        [Required]
        [StringLength(130)]
        public string PathImage { get; set; }

        public bool Active { get; set; }

        public Category Category { get; set; }

        public ICollection<DetailEntry> DetailEntries { get; set; }

        public ICollection<DetailSale> DetailSales { get; set; }
    }

    public class ArticlesList
    {
        public List<Article> ListArticles { get; set; }

        public int totalCount { get; set; }
    }

    public class ArticleDetails:Article
    {
        public string NameCategory { get; set; }
    }
}
