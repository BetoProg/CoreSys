﻿using CoreSys.Models.Valids;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreSys.Models
{
    public class Category
    {
        [Key]
        public int IdCategory { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name="Nombre")]
        public string Name { get; set; }

        [Required]
        [StringLength(120)]
        [Display(Name = "Descripcion")]
        public string Description { get; set; }

        [CheckBoolean(ErrorMessage ="Debes de hacer click en la opcion")]
        [Display(Name = "Activo")]
        public bool Active { get; set; }

        public ICollection<Article> Articles { get; set; }

        
    }

    public class CategoryList
    {
        public List<Category> ListCategory { get; set; }

        public int TotalCount { get; set; }
    }
}
