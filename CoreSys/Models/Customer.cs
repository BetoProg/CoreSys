﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreSys.Models
{
    public enum Country
    {
        Argentina,
        Brasil,
        Chile,
        Colombia,
        Ecuador,
        Mexico,
        Peru
    }

    public class Customer
    {
        [Key]
        public int IdCustomer { get; set; }

        public int IdDocument { get; set; }

        [Required]
        [StringLength(20)]
        public string CustomerType { get; set; }

        [Required]
        [StringLength(50,MinimumLength =2)]
        public string Name { get; set; }

        [Required]
        [StringLength(80,MinimumLength =2)]
        public string LastName { get; set; }

        [Required]
        [StringLength(70,MinimumLength =3)]
        public string Address { get; set; }

        [Required]
        [StringLength(50)]
        public string City { get; set; }

        [Required]
        [StringLength(50)]
        public string Region { get; set; }

        [Required]
        [StringLength(5)]
        [RegularExpression(@"/^[0-9]+$/",ErrorMessage ="Codigo Postal Invalido")]
        public string PostalCode { get; set; }

        [Required]
        [StringLength(30)]
        public Country Country { get; set; }

        [Required]
        [StringLength(10)]
        [DataType(DataType.PhoneNumber,ErrorMessage ="Telefono Invalido")]
        public string Phone { get; set; }

        [Required]
        [StringLength(80)]
        [DataType(DataType.EmailAddress,ErrorMessage ="Email invalido")]
        public string Email { get; set; }

        public DocumentType DocumentTypes { get; set; }

        public ICollection<Entry> Entries { get; set; }

        public ICollection<Sale> Sales { get; set; }
    }
}
