﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreSys.Models
{
    public class DetailEntry
    {
        [Key]
        public int IdDetailEntry { get; set; }

        public int IdEntry { get; set; }

        public int IdArticle { get; set; }

        [Required]
        [Range(1,1000)]
        public int Quantity { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal PurchasePrice { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal SalePrice { get; set; }

        public Entry Entry { get; set; }

        public Article Article { get; set; }
    }
}
