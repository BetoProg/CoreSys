﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreSys.Models
{
    public class DetailSale
    {
        [Key]
        public int IdDetailSale { get; set; }

        public int IdSale { get; set; }

        public int IdArticle { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal SalePrice { get; set; }

        [Required]
        public decimal Discount { get; set; }

        public Sale Sale { get; set; }

        public Article Article { get; set; }
    }
}
