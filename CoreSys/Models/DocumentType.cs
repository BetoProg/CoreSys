﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreSys.Models
{
    public class DocumentType
    {
        [Key]
        public int IdDocument { get; set; }

        [Required]
        public string DocumentName { get; set; }

        public ICollection<Customer> Customer { get; set; }
    }
}
