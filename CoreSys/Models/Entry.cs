﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreSys.Models
{
    public class Entry
    {
        [Key]
        public int IdEntry { get; set; }

        public int IdCustomer { get; set; }

        public int IdVoucherType { get; set; }

        [StringLength(20)]
        public string VoucherSerie { get; set; }

        [StringLength(7)]
        public string VoucherNumber { get; set; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "El campo {0} debe de ser una fecha valida")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateTime { get; set; }

        [Required]
        public decimal Tax { get; set; }

        [Required]
        public bool State { get; set; }

        public Customer Customers { get; set; }

        public VoucherType VoucherType { get; set; }

        public ICollection<DetailEntry> DetailEntries { get; set; }
    }
}
