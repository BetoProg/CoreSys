﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreSys.Models
{
    public class VoucherType
    {
        [Key]
        public int IdVoucherType { get; set; }

        [Required]
        public string VoucherName { get; set; }

        public ICollection<Entry> Entries { get; set; }

        public ICollection<Sale> Sales { get; set; }
    }
}
