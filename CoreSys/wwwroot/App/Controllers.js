﻿app.controller("categoriesController", function ($scope, servCategories) {
    $scope.ListError = [];
    $scope.ListCategories = [];

    $scope.maxSize = 5;
    $scope.totalCount = 0;
    $scope.pageIndex = 1;
    $scope.pageSizeSelected = 5;

    var getSuccessCallBack = function (result,status) {
        $scope.ListCategories = result.data.listCategory;
        $scope.totalCount = result.data.totalCount;
    };

    var addSuccessCallBack = function (result, status) {
        alert("se ha ingresado correctamente");
        $('#modalCategory').modal('hide');

        servCategories.get().then(getSuccessCallBack, errorCallBack);
    }

    var updateSuccessCallBack = function (result,status) {

        $('#modalCategory').modal('hide');

        servCategories.get().then(getSuccessCallBack, errorCallBack);
    }

    var deleteSuccessCallBack = function () {
        $('#ModalCategoryElimina').modal('hide');

        $scope.getCategories();
    }

    var errorCallBack = function (result, status, headers, config) {
        $scope.ListError = result.data;
        alert('Ocurrio un error');
    }

    $scope.getCategories = function () {
        servCategories.get($scope.pageIndex, $scope.pageSizeSelected).then(getSuccessCallBack, errorCallBack);
    }

    $scope.getCategories();

    $scope.pageChanged = function () {
        $scope.getCategories();
    };

    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getCategories();
    };

    $scope.newCategory = function () {
        $scope.category = {};
        $('#modalCategory').modal('show');
    };

    $scope.editCategories = function (c) {
        $('#modalCategoryEdit').modal('show');

        $scope.category = c;
    };

    $scope.saveCategory = function (category) {
        if (category.IdCategory == 0 || category.IdCategory === undefined) {
            servCategories.add(category).then(addSuccessCallBack, errorCallBack)
        } else {
            servCategories.update(category).then(updateSuccessCallBack, errorCallBack)
        }
    };

    $scope.eliminarCategoryModal = function (c) {
        $('#ModalCategoryElimina').modal('show');

        $scope.category = c;
    }

    $scope.EliminaCategoryFin = function (category) {
        servCategories.delete(category).then(deleteSuccessCallBack, errorCallBack);
    }
});
///////////
app.controller("articlesController", function ($scope, servArticles) {
    $scope.ListArticles = [];

    $scope.maxSize = 5;
    $scope.totalCount = 0;
    $scope.pageIndex = 1;
    $scope.pageSizeSelected = 5;


    var getSuccessCallBack = function (result, status) {
        $scope.ListArticles = result.data.listArticles;
        $scope.totalCount = result.data.totalCount;
    };

    var errorCallBack = function () {
        alert("Ocurrio un error");
    };

    var getSuccessIdCallBack = function (result, status) {
        $scope.article = result.data;
    };

    $scope.getArticles = function () {
        servArticles.get($scope.pageIndex, $scope.pageSizeSelected).then(getSuccessCallBack, errorCallBack);
    };

    $scope.newArticle = function () {
        $('#modalArticle').modal('show');
    }

    $scope.getArticles();

    $scope.pageChanged = function () {
        $scope.getArticles();
    };

    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getArticles();
    };

    $scope.detailsArticle = function (c) {
        $('#modalArticleDetail').modal('show');
        servArticles.getid(c).then(getSuccessIdCallBack,errorCallBack);
    }
}).directive('upluaderModel', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            var model = $parse(iAttrs.upluaderModel);
            var modelSetter = model.assign;

            iElement.on("change", function (e) {
                scope.$apply(function () {
                    modelSetter(scope, iElement[0].files[0]);
                });
            });
        }
    }
});
