﻿app.factory('servArticles', function ($http) {
    var url = 'api/Articles/';
    var url2 = "http://localhost:55691/api/Articles"
    return {
        get: function (pageIndex, pageSizeSelected) {
            return $http.get(url + '?pageIndex=' + pageIndex + '&pageSize=' + pageSizeSelected);
        },
        add: function (article) {
            return $http.post(url, article);
        },
        update: function (article) {
            return $http.put(url, article);
        },
        delete: function (article) {
            return $http.delete(url + article.idArticle);
        }
    }
});