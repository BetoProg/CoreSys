﻿var app = angular.module("app", ['ui.router', 'ui.bootstrap','gs.preloaded'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");

        $stateProvider.state('categories', {
            url: '/categories',
            templateUrl: 'App/templates/almacen/categories.html',
            controller: 'categoriesController'
        });

        $stateProvider.state('articles', {
            url: '/articles',
            templateUrl: 'App/templates/almacen/articles.html',
            controller: 'articlesController'
        });
});