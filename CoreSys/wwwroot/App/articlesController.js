﻿app.controller("articlesController", function ($scope, servArticles) {
    $scope.ListArticles = [];

    $scope.maxSize = 5;
    $scope.totalCount = 0;
    $scope.pageIndex = 1;
    $scope.pageSizeSelected = 5;


    var getSuccessCallBack = function (result, status) {
        $scope.ListArticles = result.data.listArticles;
        $scope.totalCount = result.data.totalCount;
    }

    var errorCallBack = function () {

    }

    $scope.getCategories = function () {
        servArticles.get($scope.pageIndex, $scope.pageSizeSelected).then(getSuccessCallBack, errorCallBack);
    }

    $scope.getArticles();

    $scope.pageChanged = function () {
        $scope.getArticles();
    };

    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.getArticles();
    };
});